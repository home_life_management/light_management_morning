# light_management_morning

朝部屋のswitchbotをon/offして部屋の明度を一定以上に維持する

* 稼働時間
  * 開始
    * 前日就寝時刻+SLEEPING_TIME_MIN分
      * 前日就寝時刻は、前日にluxが最後にBED_LUX_THRESHOLDを上回った時刻
  * 終了
    * END_TIME
* 上記で大文字で記載した環境変数はmorning.confに記載
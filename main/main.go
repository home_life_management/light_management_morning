package main

import (
	"log"
	"strconv"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/home_life_management/common_lib"
	"gitlab.com/home_life_management/common_lib/service/database"
	"gitlab.com/home_life_management/common_lib/service/switchbot"
	"gitlab.com/home_life_management/light_management_morning/config"
	"go.uber.org/zap"
)

func main() {
	c, err := config.GetConfig()
	if err != nil {
		log.Fatalf("%+v", err)
	}

	lo := &common_lib.LoggerOption{
		LogLevel: c.LogLevel,
	}
	logger := common_lib.NewLoggerWithOption(*lo)
	defer logger.Sync()
	logger.Info("morning light manager started")

	logger.Debug("start connecting to DB")
	db, err := initDB(logger)
	if err != nil {
		logger.Error(err.Error())
		return
	}
	defer db.Close()
	logger.Info("connected to DB")

	l, err := shouldLightUp(db, logger)
	if err != nil {
		logger.Error(err.Error())
		return
	}
	if l {
		ss, err := switchbot.NewSwitchBotService(c.SwitchBotMac, db, c.SwitchBotScriptPath)
		if err != nil {
			logger.Error(err.Error())
			return
		}
		err = ss.Toggle()
		if err != nil {
			logger.Error(err.Error())
			return
		}
	}

	logger.Info("morning light manager end successfully")
}

func initDB(logger *zap.Logger) (database.DatabaseServiceProvider, error) {
	c, _ := config.GetConfig()

	do := &database.DatabaseOption{
		Name:     c.DBName,
		User:     c.DBUser,
		Password: c.DBPassword,
		Host:     c.DBHost,
		Port:     c.DBPort,
	}
	d, err := database.NewPostgresService(do)
	if err != nil {
		return nil, errors.Wrap(err, "failed to init postgres service")
	}

	err = d.Connect()
	if err != nil {
		return nil, errors.Wrap(err, "failed to connect to DB")
	}

	return d, nil
}

func shouldLightUp(db database.DatabaseServiceProvider, logger *zap.Logger) (bool, error) {
	c, _ := config.GetConfig()

	// 朝の指定時刻(END_TIME)より後であればfalse
	now, _ := time.Parse("15:04", time.Now().Format("15:04"))
	if !now.Before(c.EndTime) {
		logger.Info("not morning", zap.String("morning_end", c.EndTime.Format("15:04")))
		return false, nil
	}

	// 昨日の就寝時刻から指定時間経過していなければfalse
	logger.Debug("start getting last night bed time")
	bedTime, err := db.GetBedTime(time.Now().AddDate(0, 0, -1), c.BedLuxThreshold)
	if err != nil {
		err = errors.Wrap(err, "failed to get last night bed time")
		logger.Error(err.Error())
		return false, err
	}
	logger.Info("got last night bed time", zap.String("bed_time", bedTime.Format("15:04")))

	if time.Now().Before(bedTime.Add(time.Minute * time.Duration(c.SleepingTimeMin))) {
		logger.Info("not time to light up")
		return false, nil
	}

	// 現在の明るさが閾値以上ならfalse
	logger.Debug("start getting sensor data")
	r, err := db.GetLatestRpzSensorLog()
	if err != nil {
		err = errors.Wrap(err, "failed to get latest rpz sensor log")
		logger.Error(err.Error())
		return false, err
	}
	logger.Info("got sensor data", zap.String("lux", strconv.FormatFloat(r.Lux, 'f', -1, 64)))
	if r.Lux < float64(c.LightUpLuxThreshold) {
		logger.Info("time to light up")
		return true, nil
	}
	logger.Info("already bright enough", zap.String("lux_threshold", strconv.Itoa(c.LightUpLuxThreshold)))

	return false, nil
}

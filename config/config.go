package config

import (
	"os"
	"time"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"gitlab.com/home_life_management/common_lib"
)

type Config struct {
	DBName     string `mapstructure:"DB_NAME"`
	DBUser     string `mapstructure:"DB_USER"`
	DBPassword string `mapstructure:"DB_PASSWORD"`
	DBHost     string `mapstructure:"DB_HOST"`
	DBPort     int    `mapstructure:"DB_PORT"`

	BedLuxThreshold     int    `mapstructure:"BED_LUX_THRESHOLD"`
	SleepingTimeMin     int    `mapstructure:"SLEEPING_TIME_MIN"`
	LightUpLuxThreshold int    `mapstructure:"LIGHT_UP_LUX_THRESHOLD"`
	SwitchBotMac        string `mapstructure:"SWITCH_BOT_MAC"`
	SwitchBotScriptPath string `mapstructure:"SWITCH_BOT_SCRIPT_PATH"`
	EndTime             time.Time

	LogLevel common_lib.LogLevel
}

var conf *Config

func GetConfig() (*Config, error) {
	if conf == nil {
		err := initConfig()
		if err != nil {
			return nil, err
		}
	}
	return conf, nil
}

func initConfig() error {
	viper.BindEnv("DB_NAME")
	viper.BindEnv("DB_USER")
	viper.BindEnv("DB_PASSWORD")
	viper.BindEnv("DB_HOST")
	viper.BindEnv("DB_PORT")
	viper.BindEnv("BED_LUX_THRESHOLD")
	viper.BindEnv("SLEEPING_TIME_MIN")
	viper.BindEnv("LIGHT_UP_LUX_THRESHOLD")
	viper.BindEnv("SWITCH_BOT_MAC")
	viper.BindEnv("SWITCH_BOT_SCRIPT_PATH")

	err := viper.Unmarshal(&conf)
	if err != nil {
		return errors.Wrap(err, "failed to unmarshal config file")
	}
	err = mapEndTime()
	if err != nil {
		return err
	}
	mapLogLevel()

	return nil
}

func mapEndTime() error {
	e := os.Getenv("END_TIME")
	end, err := time.Parse("15:04", e)
	if err != nil {
		// TODO: config.go内でエラーが発生した場合に、エラー出力にファイル名、行数が出力されないので対策検討
		return errors.Wrap(err, "failed to parse environment variable END_TIME")
	}
	conf.EndTime = end

	return nil
}

// viperでstringをconst variableにmapする方法が不明なため実装
func mapLogLevel() {
	l := os.Getenv("LOG_LEVEL")
	switch l {
	case "DEBUG":
		conf.LogLevel = common_lib.Debug
	case "INFO":
		conf.LogLevel = common_lib.Info
	case "WARN":
		conf.LogLevel = common_lib.Warn
	case "ERROR":
		conf.LogLevel = common_lib.Error
	default:
		conf.LogLevel = common_lib.Info
	}
}
